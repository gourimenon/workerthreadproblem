package com.company;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DbOperations extends Locationthread {
    Connect connect;
    Connection connection;

   public DbOperations() {

   }
    public void databaseOperations(String textfile, String size,String strLine) throws SQLException
    {   connect = new Connect();
        connection = connect.createConnection();

        PreparedStatement statement = connection.prepareStatement("INSERT textfile(FileName,Size,Content) values (?,?,?)");

        statement.setString(1, textfile);
        statement.setString(2, size);
        statement.setString(3, strLine);

        statement.execute();
    }


}
